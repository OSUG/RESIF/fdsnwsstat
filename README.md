# Statistiques sur les webservices FDSN

Récupère un message sur la file AMQP et analyse les requêtes de type station et dataselect :
  - lance dataselect avec les informations récupérées
  - analyse la métadonnée au format texte ou stationXML
puis enregistre une statistique dans postgres.

## Utilisation

Le programme se configure avec les variables d'environnements suivantes :

  - `RUNMODE` : Si `dryrun` alors le programme ne consomme pas les messages et n'écrit pas en base. Si `production`, alors les logs sont mis au niveau `INFO`
  - `AMQP_BROKER` : URL du broker AMQP. Par défaut : `amqp-geodata.ujf-grenoble.fr:5672`
  - `AMQP_QUEUE` : Nom de la queue AMQP. Par défaut: `fdsnwsstat`
  - `STATS_DBURI` : URI de la base de donnée postgres pour les statistiques. Par défaut `postgresql://dataselectstat@resif-pgpreprod.u-ga.fr:5432/resifstats`
  - `INV_DBURI` : URI pour la base de données resifInv. Par défaut `postgresql://dataselectstat@resif-pgprod.u-ga.fr:5432/resifInv-Prod`
  - `AUTH_DBURI` : URI pour la base de données resifAuth. Par défaut `postgresql://dataselectstat@resif-pgprod.u-ga.fr:5432/resifAuth`

## Déploiement
### Table postgresql

```sql
CREATE table dataselectvol (
  date timestamp without time zone,
  requestid varchar(8),
  hostname varchar(64),
  useragent text,
  clientip varchar(15),
  userid bigint,
  username varchar(32),
  geohash varchar(12),
  country varchar(2),
  network varchar(8),
  station varchar(6),
  location varchar(2),
  channel varchar(3), 
  bytes bigint,
  nbfiles bigint);
CREATE table stationrequests (
  date timestamp without time zone,
  requestid varchar(8),
  hostname varchar(64),
  useragent text,
  clientip varchar(15),
  geohash varchar(12),
  country varchar(2),
  network varchar(8),
  station varchar(6),
  location varchar(2),
  channel varchar(3),
  bytes integer
```

- `requestid` est l'identifiant de requête généré par l'extracteur ws-dataselect
- `hostname` est le serveur sur lequel à tourné l'extracteur
- `useragent` est l'agent renseigné par le client ayant initié la requête dataselect
- `clientip` est l'adresse IP du client
- `userid` est un hash de l'adresse IP calculé ainsi :  `int(md5(userstring.encode('utf8')).hexdigest()[:8], 16)`
- `username` est le nom d'utilisateur lors de la requête queryauth
- `geohash` est le hash géographique calculé par dataselectstat à partir de l'adresse IP
- `country` le pays en 2 lettres
- `network`, `station`, `location`, `channel`
- `bytes` la quantité de données livrées
- `nbfiles` le nombre de fichiers ouvert pour cette portion de requête

### Autorisations sur les bases de données Résif

Les privilèges suivants doivent être configurés:

  - Base resifInv-Prod, SELECT sur la table networks
  - Base resifAuth, SELECT sur la table users
  - Base resifstats, INSERT sur les tables dataselectvol et stationrequests
  
### Construire le docker et le publier
```
pipenv lock -r > requirements.txt
docker build -t gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/dataselectstat .
docker login gricad-registry.univ-grenoble-alpes.fr
docker push gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/dataselectstat
docker logout
```

### Docker preprod
```
docker run --rm -e PGPASSWORD="MOTDEPASSEresifstats" gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/dataselectstat .
```

### Docker prod
```
docker run --rm -e PGPASSWORD="MOTDEPASSEresifstats" gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/dataselectstat .
```

## Tester

### Pytest

Quelques tests unitaires ont été implémentés :

    pytest -s

### Envoyer un message

le script `test_json.sh` devrait vous aider à envoyer un message bien formé.

Exemple de message : 

```json
{
  "timestamp": "2020-05-29T12:06:32Z",
  "requestid": "345ERRTT",
  "hostname": "plop.out.fr",
  "useragent": "Coucou \"j'ai plein de quotes\"",
  "clientip": "123.123.123.123",
  "user": "lagaffeg",
  "options": "-Ps -msl 0.0",
  "matchlist": "* CURIE * * * 2020,125,06,00,00,000000 2020,134,13,57,04,000000\n",
  "files": "/mnt/auto/archive/data/byyear/2020/FR/CURIE/HHE.D/FR.CURIE.00.HHE.D.2020.125 /mnt/auto/archive/data/byyear/2020/FR/CURIE/LHZ.D/FR.CURIE.00.LHZ.D.2020.127 /mnt/auto/archive/rawdata/bud/2020/FR/CURIE/HHZ.D/FR.CURIE.00.HHZ.D.2020.128 /mnt/auto/archive/rawdata/bud/2020/FR/CURIE/LHN.D/FR.CURIE.00.LHN.D.2020.134 "
}
```
