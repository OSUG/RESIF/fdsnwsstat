#!/usr/bin/env python3

from io import StringIO
import psycopg2
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from geolite2 import geolite2
import geohash2


# Elasticsearch database access
dburi='postgres://resifstats@resif-pgdev.u-ga.fr:5433/resifstats'
es = Elasticsearch(['resif-elk.u-ga.fr'], port=9200)
count = es.count(index="logstash-dataselectlog-*", body={"query": {"match_all": {}}})
print(count)
res = Search(using=es, index="logstash-dataselectlog-*")
# TODO : une première requête des entrées qui n'ont pas tag:{'_grokparsefailure}
# TODO : une seconde requête avec ceux qui ont _grokparsefailure pour rescanner tout.
georeader = geolite2.reader()
# Créer une chaine de caractère avec tous les éléments séparés par \t
items = []
print("Constructing a huge copy string on %d records"%(count['count'])

georeader = geolite2.reader()
missed = 0
for hit in res.scan():
    e = hit.to_dict()
    nbfiles = 0
    useragent = ""
    if 'User_Agent' in e.keys():
        useragent = e['User_Agent']
    if 'nbfiles' in e.keys():
        nbfiles = e['nbfiles']
    location = georeader.get(e['clientip'])
    if location != None:
        e['geohash'] = geohash2.encode(location['location']['latitude'], location['location']['longitude'])
    else:
        e['geohash'] = None

    try:
        items.append('\t'.join((e['@timestamp'],
                                e['id'],
                                e['host']['name'],
                                useragent,
                                e['clientip'],
                                e['network'],
                                e['station'],
                                e['location'],
                                e['channel'],
                                str(e['bytes']),
                                str(nbfiles),
                                geohash))+'\n')
    except Exception as err:
      missed = missed+1
      print(err)
      print(e)
      print("Missed : %d"%(missed))
      print("Sending the huge copy string to database")
      strio = StringIO()
      strio.writelines(items)
      strio.seek(0)
try:
      conn = psycopg2.connect(dburi)
      cur = conn.cursor()
      cur.copy_from(strio, 'dataselectvol')
      cur.close()
      conn.commit()
      conn.close()
except Exception as e:
      print("Error writing to postgres %s database"%(dburi))
      print(e)

print("Wrote %d entries"%(len(items)))
