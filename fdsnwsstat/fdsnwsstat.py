import os
import re
import logging
from io import StringIO
from tempfile import TemporaryDirectory
import subprocess
import json
from xml.etree import ElementTree
from hashlib import md5
import psycopg2
import geoip2.database
import geohash2
from fdsnnetextender import FdsnNetExtender
import shutil
from redis.backoff import ExponentialBackoff
from redis.retry import Retry
from redis.client import Redis
from redis.exceptions import (
    BusyLoadingError,
    ConnectionError,
    TimeoutError,
)


if os.getenv("RUNMODE") == "production":
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(message).120s", level=logging.INFO
    )
else:
    logging.basicConfig(
        format="%(asctime)s [%(filename)s:%(lineno)s - %(funcName)20s()] %(levelname)s %(message)s",
        level=logging.DEBUG,
    )
logger = logging.getLogger(__name__)
# Dryryn allows to not consume the message from queue and not register anything to database.
dryrun = False
if os.getenv("RUNMODE") == "dryrun":
    dryrun = True

redis_server = os.getenv("REDIS_HOST", "localhost")
redis_port = int(os.getenv("REDIS_PORT", 6379))
max_retries = int(os.getenv("REDIS_MAX_RETRIES", 3))
queue = os.getenv("REDIS_QUEUE", "fdsnwsstat-test")
# SPOOLDIR est le répertoire dans lequel on pourra trouver la donnée à analyser
spooldir = os.getenv("SPOOL_DIR", "/spool")
stats_dburi = os.getenv(
    "STATS_DBURI", "postgresql://fdsnwsstat@resif-pgpreprod.u-ga.fr:5432/resifstats"
)
inv_dburi = os.getenv(
    "INV_DBURI", "postgresql://fdsnwsstat@resif-pgprod.u-ga.fr:5432/resifInv-Prod"
)
auth_dburi = os.getenv(
    "AUTH_DBURI", "postgresql://fdsnwsstat@resif-pgprod.u-ga.fr:5432/resifAuth"
)
useragent_exclude_exp = re.compile(os.getenv("USERAGENT_EXCLUDE_EXP", "(?!)"))

network_extender = FdsnNetExtender()
georeader = geoip2.database.Reader(
    os.path.dirname(os.path.realpath(__file__)) + "/GeoLite2-City.mmdb"
)


def eval_station_xml(xml):
    """
    From a stationXML string, analyze wich metadata is concerned
    and return list of metadata constructed as a dictionary
    {network: "NET", station: "STA", location: "LOC", channel: "CHA"}
    """
    metadata = []
    # parse the XML string
    try:
        stationxml = ElementTree.fromstring(xml)
    except ElementTree.ParseError as e:
        logger.error(e)
        raise (ValueError("Malformated XML data"))
    for net in stationxml.findall("{http://www.fdsn.org/xml/station/1}Network"):
        logger.debug("Network %s", net.get("code"))
        stations = net.findall("{http://www.fdsn.org/xml/station/1}Station")
        extnet = network_extender.extend(net.get("code"), net.get("startDate"))
        if len(stations) == 0:
            metadata.append(
                {
                    "network": extnet,
                    "station": "",
                    "location": "",
                    "channel": "",
                    "size": len(xml),
                    "files": 1,
                }
            )
        for sta in stations:
            channels = sta.findall("{http://www.fdsn.org/xml/station/1}Channel")
            if len(channels) == 0:
                metadata.append(
                    {
                        "network": extnet,
                        "station": sta.get("code"),
                        "location": "",
                        "channel": "",
                        "size": len(xml),
                        "files": 1,
                    }
                )
            for chan in channels:
                metadata.append(
                    {
                        "network": extnet,
                        "station": sta.get("code"),
                        "location": chan.get("locationCode"),
                        "channel": chan.get("code"),
                        "size": len(xml),
                        "files": 1,
                    }
                )
    logger.debug(metadata)
    return metadata


def eval_station_txt(stationtxt):
    """
    From a string, analyze wich metadata is concerned
    and return list of metadata constructed as a dictionary
    {network: "NET", station: "STA", location: "LOC", channel: "CHA"}
    """
    metadata = []
    # From the header, guess the right level and set the appropriate regex
    lines = stationtxt.rstrip().split("\n")
    if "|Channel|" in lines[0]:
        logger.debug("Channel level metadata")
        regex = r"^(?P<net>[0-9A-Z]{1,2})\|(?P<station>[0-9A-Z]{3,5})\|(?P<location>[0-9]{2}|)\|(?P<channel>[FGDCESHBMLVURPTQAO][HLGMN][ZNEABC123TRUVW])\|.*\|(?P<startyear>[12][0-9]{3}).*\|.*$"
    elif "|Station|" in lines[0]:
        logger.debug("Station level metadata")
        regex = r"^(?P<net>[0-9A-Z]{1,2})\|(?P<station>[0-9A-Z]{3,5})\|.*\|.*\|.*\|.*\|(?P<startyear>[12][0-9]{3}).*\|.*$"
    else:
        logger.debug("Network level metadata")
        regex = r"^(?P<net>[0-9A-Z]{1,2})\|.*\|(?P<startyear>[12][0-9]{3}).*\|.*$"
    for l in lines[1:]:
        logger.debug("Parsing line %s", l)
        match = re.search(regex, l)
        if match:
            md = {}
            matchgroups = match.groupdict()
            if "net" in matchgroups.keys():
                extnet = network_extender.extend(
                    matchgroups["net"], matchgroups["startyear"]
                )
                md = {"network": extnet, "station": "", "location": "", "channel": ""}
            if "station" in matchgroups.keys():
                md["station"] = matchgroups["station"]
            if "channel" in matchgroups.keys():
                md["location"] = matchgroups["location"]
                md["channel"] = matchgroups["channel"]
            md["size"] = len(stationtxt)
            md["nbfiles"] = 1
            metadata.append(md)
    logger.debug(metadata)
    return metadata


def run_dataselect(options, matchlist, files):
    """
    This function runs dataselect on all files in array files, whith options and matchlist to return the size of the data extracted.
    options : string for the options to pass to dataselect
    matchlist : string containing the matchlist
    files : array of files to get with dataselect
    """
    logger.debug("Analysing data content in %s", files)
    sizes = []
    with TemporaryDirectory() as workdir:
        if matchlist != "":
            with open(f"{workdir}/match.list", "w") as matchfile:
                matchfile.write(matchlist)
            options = options + f" -s {workdir}/match.list"
        # Write data in an ordered way
        for datafile in files:
            if not os.access(datafile, os.R_OK):
                logger.info("File %s not readable, skipping", datafile)
                continue
            options = " ".join(options.split())
            command = (
                f"dataselect {options} {datafile} -A {workdir}/%Y.%n.%s.%l.%c.msdata"
            )
            logger.info("Running %s", command)
            try:
                subprocess.run(command.split(" "), check=True)
            except subprocess.CalledProcessError as err:
                logger.error(err)

        # Pour chaque fichier msdata, prendre ses références et sa taille.
        # Construire un tableau sizes = [ {network: , station: , location: , channel: , size: }, ]
        for msdata in os.listdir(workdir):
            if msdata.endswith(".msdata"):
                # Get references
                (year, network, station, location, channel) = msdata.split(".")[0:5]
                if not location:
                    location = "--"
                sizes.append(
                    {
                        "network": network_extender.extend(network, year),
                        "station": station,
                        "location": location,
                        "channel": channel,
                        "size": os.stat(f"{workdir}/{msdata}").st_size,
                    }
                )
    logger.debug(sizes)
    return sizes


def add_geohash(data):
    data["geohash"] = None
    data["country"] = ""
    try:
        location = georeader.city(data["clientip"])
    except (ValueError, geoip2.errors.AddressNotFoundError) as err:
        logger.warning(err)
        location = None
        return data
    try:
        data["geohash"] = geohash2.encode(
            location.location.latitude, location.location.longitude
        )
    except Exception as err:
        logger.info(
            "Error computing a geohash with latitude %s and longitude %s",
            location.location.latitude,
            location.location.longitude,
        )
        logger.info(err)

    if location.country.iso_code:
        data["country"] = location.country.iso_code
    elif location.registered_country.iso_code:
        logger.debug("No country provided")
        data["country"] = location.registered_country.iso_code
    else:
        logger.info(
            "No country metadata in maxminddb city database for %s", data["clientip"]
        )
    return data


def add_userid(data):
    """
    Compute a userid string just the same way as all EIDA nodes do with seiscomp
    - try to get the email from the user parameter
    - if we have it, hash it allong with the ip address
    - if we don't have it, hash only the ipadress
    https://github.com/SeisComP/main/blob/3c11014d49dbefd29ccaa436647cb1aa0727482f/apps/fdsnws/fdsnws/reqlog.py#L36
    """
    userstring = data["clientip"]
    data["auth"] = False
    if "user" in data.keys() and data["user"] != "anonymous":
        data["auth"] = True
        try:
            with psycopg2.connect(auth_dburi) as conn:
                with conn.cursor() as curs:
                    curs.execute(
                        "select email from users where login=%s", (data["user"],)
                    )
                    if curs.rowcount == 0:
                        logger.debug(
                            "No email found for user %s. Using client IP %s",
                            data["user"],
                            data["clientip"],
                        )
                        userstring = data["clientip"]
                    else:
                        userstring = curs.fetchone()[0].lower()
                        logger.debug(
                            "%s is attached to email %s", data["user"], userstring
                        )
        except psycopg2.Error as err:
            logger.error("Problem connecting to authentication database")
            logger.error(err)

    data["userid"] = int(md5(userstring.encode("utf8")).hexdigest()[:8], 16)
    logger.debug("userid computed: %s", data["userid"])
    return data


def register_stat(data):
    """
    metadata is a list f dictionaries to insert into table.
    """
    logger.debug("Starting stat registration %s", len(data["sizes"]))
    # string to be used by psycopg2 cursor.copy_from()
    to_insert = StringIO("")
    # List of common columns for stationrequests and dataselectvol
    columns = (
        "date",
        "requestid",
        "hostname",
        "useragent",
        "clientip",
        "geohash",
        "country",
        "network",
        "station",
        "location",
        "channel",
        "bytes",
    )
    if data["ws"] == "station":
        table = "stationrequests"
        # La taille totale d'une métadonnée apprait pour chacun de ses éléments. On veut donc la partager
        # bytes_divider permet de partager la taille indiquée dans une métadonnée de type station entre toutes les métadonnées
        bytes_divider = len(data["sizes"])
    else:
        table = "dataselectvol"
        columns += ("nbfiles", "userid", "auth")
        # Là on divisera par 1 parceque c'est déjà bien comptabilisé
        bytes_divider = 1

    # Prepare the postgres COPY format by changing empty values to "\N"
    # Maybe there is a more efficient way ?
    for k in data.keys():
        if data[k] == "":
            data[k] = r"\N"
    for size in data["sizes"]:
        for k in size.keys():
            if size[k] == "":
                size[k] = r"\N"
        to_insert.write(
            f"{data['timestamp']}\t{data['requestid'][:8]}\t{data['hostname']}\t{data['useragent']}\t{data['clientip']}\t{data['geohash']}\t{data['country']}\t{size['network']}\t{size['station']}\t{size['location']}\t{size['channel']}\t{int(size['size']/bytes_divider)}"
        )
        if data["ws"] == "dataselect":
            to_insert.write(f"\t{data['nbfiles']}\t{data['userid']}\t{data['auth']}")
        to_insert.write("\n")
    logger.debug(to_insert.getvalue())
    to_insert.seek(0)
    try:
        with psycopg2.connect(stats_dburi) as conn:
            with conn.cursor() as curs:
                ret = curs.copy_from(to_insert, table, columns=columns)
                logger.debug("Return from COPY FROM: %s", ret)
            conn.commit()
    except psycopg2.Error as err:
        logger.error(err)
        return False
    logger.debug("Stats registered")
    return True


def nslc_deterministic_or_empty(nslc):
    """
    Returns the input string if it determines one of the NSLC caracteristics.
    Returns '' if it matches more than one.
    """
    rs = nslc
    if re.match(r"[\[\?\*]", nslc):
        rs = ""
    return rs


def expand_network_for_nodata(net, datestring):
    if re.match(r"[\[\?\*]", net):
        return ""
    try:
        startyear = datestring.split(",")[0]
        extnet = network_extender.extend(net, startyear)
        return extnet
    except:
        # Something wrong ? we did our best, return empty string
        return ""


def dataselect_no_data(matchlist):
    """
    Process an event that did not return any data.
    Parse the matchlist string to get the selections
    Register one element per line
    matchlist looks like this:
    GL * * * 2014-02-18T09:27:45 2014-02-18T09:31:00
    MQ * * * 2014-02-18T09:27:45 2014-02-18T09:31:00
    WI * * * 2014-02-18T09:27:45 2014-02-18T09:31:00
    PF * * * 2014-02-18T09:27:45 2014-02-18T09:31:00
    Replace "*" with empty string.
    Set bytes and nbfiles to 0
    Returns an array of dictionary to register to the database
    """
    # Il faut faire l'expansion de chaque ligne ... pfff
    # Si un paramètre n'est pas complètement déterminé, alors on met une chaine vide.
    sizes = []
    if len(matchlist) == 0:
        logger.debug("Matchlis is empty, register an empty request")
        sizes = [
            {"network": "", "station": "", "location": "", "channel": "", "size": 0}
        ]
    for line in matchlist.splitlines():
        logger.debug("Matchlist line: %s", line)
        # On extrait les 4 premiers éléments de la ligne, les suivants ne nous intéressent pas.
        (n, s, l, c, _, start, _) = line.split()
        sizes.append(
            {
                "network": expand_network_for_nodata(n, start),
                "station": nslc_deterministic_or_empty(s),
                "location": nslc_deterministic_or_empty(l),
                "channel": nslc_deterministic_or_empty(c),
                "size": 0,
            }
        )
    return sizes


def is_useragent_blacklisted(agent):
    """
    Test if the string matches the blacklisting regular expression for the useragents
    The blacklisting regular expression comes from USERAGENT_EXCLUDE_EXP environment variable
    params:
    - agent: the useragent string to test
    returns:
       - True if useragent is blacklisted
       - False if useragent is not blacklisted
    """
    # Filter out blacklisted useragents
    # This is used to avoid registering statistics from monitoring activity
    return useragent_exclude_exp.search(agent) is not None


# Message is a JSON format like :
# {
#   "timestamp": "2020-05-14T15:35:43Z",
#   "user": "7HCE...",
#   "requestid": "345ERRTT",
#   "hostname": "plop.out.fr",
#   "useragent": "Coucou \"j'ai plein de quotes\"",
#   "clientip": "123.123.123.123",
#   "options": "-k -x -h",
#   "matchlist": "FR RUSF * HHE  *  2019,201,00,00,05,000000 2019,202,00,00,05,000000\nFR RUSF * HHN  *  2019,201,00,00,05,000000 2019,202,00,00,05,000000\nFR RUSF * HHZ  *  2019,201,00,00,05,000000 2019,202,00,00,05,000000\n\n",
#   "files": "file1 file2 file3"
# }
def fdsnws_process(message):
    """
    Process the request by : (1) Running dataselect and (2) registering result in database
    Returns number of files processed or -1 if something went wrong in the message format.
    Returns -2 if the message could not be registered to database
    """
    data = {}
    logger.debug("Processing %s", message)
    try:
        data = json.loads(message)
    except json.JSONDecodeError as err:
        logger.error(err)
        logger.debug(message)
        return -1

    if is_useragent_blacklisted(data["useragent"]):
        logger.info(
            "Useragent %s matched blacklist expression. Ignoring this request",
            data["useragent"],
        )
        return 0

    if data["ws"] == "station":
        data["nbfiles"] = 0
        if data["format"] == "xml":
            try:
                data["sizes"] = eval_station_xml(data["result"])
            except ValueError:
                return -1
        else:
            data["sizes"] = eval_station_txt(data["result"])
    elif data["ws"] == "ph5ws-dataselect":
        # Stats comes from ph5ws-dataselect and is already computed
        data["nbfiles"] = 0
        data["ws"] = "dataselect"
        data["useragent"] = "ph5ws-dataselect"
        data["hostname"] = ""
        data = add_userid(data)
    elif data["ws"] == "dataselect":
        logger.info("Statistics for dataselect")
        data = add_userid(data)
        # Stats comes from wsdataselect and has to be computed
        if "nbfiles" in data.keys() and int(data["nbfiles"]) == 0:
            logger.debug("The request did not deliver any data")
            data["sizes"] = dataselect_no_data(data["matchlist"])
        else:
            logger.debug("The request delivered data")
            seedfile = f"{spooldir}/{data['requestid']}/delivered.seed"
            if os.path.exists(seedfile):
                data["sizes"] = run_dataselect(
                    "-Ps -msl 0.0", data["matchlist"], [seedfile]
                )
                try:
                    shutil.rmtree(
                        os.path.join(spooldir, data["requestid"]), ignore_errors=False
                    )
                except Exception:
                    logger.error(
                        "Could not clean spool directory %s/%s",
                        spooldir,
                        data["requestid"],
                    )

            else:
                logger.error("Nothing found at %s", seedfile)
                return -1
    else:
        logger.error("Service %s not supported.", {data["ws"]})
        return -1

    # Usergagent : on ne prend que la partie significative, c'est à dire tout ce qu'il y a avant l'espace.
    # Cette regex capture le premier mot et s'il y a un / capture aussi le numéro de version
    regex = r"^([^\s\/]+)\/*([^\s]+)*.*$"
    match = re.search(regex, data["useragent"])
    if match:
        data["useragent"] = match[1]

    data = add_geohash(data)
    logger.debug(data)
    if dryrun:
        logger.info("DRYRUN, not registering to database")
    else:
        # Un peu maladroit ...
        logger.info(
            "Registering %s, %s entries to database",
            data["requestid"],
            len(data["sizes"]),
        )
        ret = register_stat(data)
        if not ret:
            return -2
    return data["nbfiles"]


if __name__ == "__main__":
    logger.info("Starting fdsnwsstats with retry: %s", max_retries)
    retry = Retry(ExponentialBackoff(), max_retries)
    try:
        r = Redis(
            host=redis_server,
            port=redis_port,
            db=0,
            retry=retry,
            retry_on_error=[BusyLoadingError, ConnectionError, TimeoutError],
        )
    except:
        logger.error("Connection refused")
        exit(1)

    while True:
        (q, message) = r.blpop(queue)
        logger.info("Receiving message %s from queue %s", message, q)
        fdsnws_process(message.decode("utf8"))
