#!/usr/bin/env python3

from fdsnwsstat.fdsnwsstat import eval_station_xml, eval_station_txt


def test_eval_station_xml_channel():
    """
    Test a valid stationXML evaluation with 514 channels
    """
    stationxml_fd = open("tests/stationxml_test0.xml", 'r')
    stationxml = stationxml_fd.read()
    metadata = eval_station_xml(stationxml)
    assert len(metadata) == 515

def test_eval_station_txt_channel():
    """
    Test a valid text metadata file with 308 channels
    """
    station_fd = open("tests/G_channels.txt", 'r')
    station = station_fd.read()
    metadata = eval_station_txt(station)
    assert len(metadata) == 309

def test_eval_station_xml_station():
    """
    Test a valid stationXML evaluation with 514 channels
    """
    stationxml_fd = open("tests/stationxml_3C_stations.xml", 'r')
    stationxml = stationxml_fd.read()
    metadata = eval_station_xml(stationxml)
    assert len(metadata) == 515

def test_eval_station_txt_station():
    """
    Test a valid text metadata file with 308 channels
    """
    station_fd = open("tests/stationxml_3C_stations.txt", 'r')
    station = station_fd.read()
    metadata = eval_station_txt(station)
    assert len(metadata) == 309

def test_eval_station_xml_network():
    """
    Test a valid stationXML evaluation with 514 channels
    """
    stationxml_fd = open("tests/stationxml_network.xml", 'r')
    stationxml = stationxml_fd.read()
    metadata = eval_station_xml(stationxml)
    assert len(metadata) == 515

def test_eval_station_txt_network():
    """
    Test a valid text metadata file with 308 channels
    """
    station_fd = open("tests/stationxml_3C_stations.txt", 'r')
    station = station_fd.read()
    metadata = eval_station_txt(station)
    assert len(metadata) == 309
