#!/usr/bin/env bash

HOSTNAME="plop.out.fr"
USERAGENT="spaces and special ''\""
IPADDRESS=147.171.168.150
AMQPQUEUE=fdsnwsstat
tident=ZZZZZZ
json_template='{"ws":"station","timestamp":"%s","requestid":"%s","hostname":"%s","useragent":%s,"clientip":"%s","result":%s,"format":"%s"}\n'
# jsonification du résultat
result_to_json=$(wget -q -O - "http://ws.resif.fr/fdsnws/station/1/query?net=RD&sta=PGF&level=channel&format=xml"|jq -aRs .)
useragent_to_json=$(echo -n $USERAGENT|jq -aRs .)
printf $json_template "$(date +%FT%TZ)" "$tident" "$HOSTNAME" "${useragent_to_json}" "$IPADDRESS" "${result_to_json}" "xml" > amqp_message.json
echo "Sending to $AMQPQUEUE"
cli-proton-python-sender --broker-url "amqp-geodata.ujf-grenoble.fr:5672/${AMQPQUEUE}" --msg-content-from-file amqp_message.json
echo "RC: $?"

