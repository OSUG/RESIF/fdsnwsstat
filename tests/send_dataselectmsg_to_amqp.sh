REQUESTID="345ERRTT"
HOSTNAME="plop.out.fr"
USERAGENT="Coucou \"j'ai plein de quotes\""
IPADDRESS=0.0.0.0
WORK=.
option="-Ps  -msl 0.0"

echo "=== Première option en pure bash ==="
set -f
# On prend dans une variable le contenu du fichier matchlist, mais on veut garder les \n alors il y a ce truc un peu idiot, mais j'ai pas trouvé mieux
MATCHLIST="$(cat $WORK/match.list|tr '\n' '|' | sed 's/|/\\n/g')"
#MATCHLIST=$(cat $WORK/match.list)
DATAFILES=$(cat $WORK/bud.lst $WORK/archive.lst | sort -u | tr '\n' ' ')
echo $MATCHLIST
json_template='{"timestamp":"%s","requestid":"%s","hostname":"%s","useragent":"%s","clientip":"%s","options":"%s","matchlist":"%s","files":"%s"}' 
message=$(printf $json_template "$(date +%FT%TZ)" "$REQUESTID" "$HOSTNAME" "${USERAGENT//\"/\\\"}" "$IPADDRESS" "$option" "${MATCHLIST%|}" "${DATAFILES}")
echo $message
set +f

cli-proton-python-sender --broker-url "amqp-geodata.ujf-grenoble.fr:5672/fdsnwsstat-test" --msg-content "$message" --log-msgs body
echo $?



# echo "=== Deuxième option avec l'outil jq ==="
# # Un peu mieux, mais dépendant de l'installation de jq
# jq -n \
#     --arg ri $REQUESTID \
#     --arg host $HOSTNAME \
#     --arg ua "$USERAGENT" \
#     --arg ci $IPADDRESS \
#     --arg ts $(date +%FT%TZ) \
#     --arg files "${DATAFILES}" \
#     --rawfile match $WORK/match.lst \
#     '{timestamp: $ts, requestid: $ri, hostname: $host, useragent: $ua, clientip: $ci, matchlist: $match, files: $files}'
# 
