#############################################
FROM ghcr.io/astral-sh/uv:python3.12-bookworm-slim AS dataselect-builder
# Dataselect copilation
WORKDIR /src
RUN apt-get update \
  && apt-get install -y --no-install-recommends build-essential curl inotify-tools wget  ca-certificates \
  && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
  && apt-get clean
ARG DATASELECT_VERSION=4.0.1
RUN echo ${DATASELECT_VERSION}
RUN wget https://github.com/EarthScope/dataselect/archive/refs/tags/v${DATASELECT_VERSION}.tar.gz
RUN tar xf v${DATASELECT_VERSION}.tar.gz
WORKDIR /src/dataselect-${DATASELECT_VERSION}
RUN make
RUN cp dataselect /bin
# Enable bytecode compilation
# Copy from the cache instead of linking since it's a mounted volume
#############################################
FROM ghcr.io/astral-sh/uv:python3.12-bookworm-slim AS pybuilder
ENV UV_COMPILE_BYTECODE=1 \
    UV_LINK_MODE=copy
WORKDIR /app
COPY pyproject.toml uv.lock ./
# Sync only project dependencies
#RUN --mount=type=cache,target=/root/.cache/uv \
RUN uv sync --frozen --no-install-project --no-dev
#############################################
FROM docker.io/library/python:3.12-slim
COPY --from=dataselect-builder /bin/dataselect /bin/dataselect
RUN addgroup --system app && \
    adduser --system --group app
USER app
WORKDIR /app
COPY --from=pybuilder --chown=app:app /app /app
COPY . .
ARG CI_COMMIT_SHORT_SHA
ENV COMMIT=$CI_COMMIT_SHORT_SHA \
    PATH="/app/.venv/bin:$PATH"
CMD ["python" , "/app/fdsnwsstat/fdsnwsstat.py"]
